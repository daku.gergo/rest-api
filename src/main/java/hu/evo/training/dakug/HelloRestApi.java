package hu.evo.training.dakug;

import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/hello")
public class HelloRestApi {

    @POST
    @Path("/{name}/1")
    public Response sayHello(@PathParam("name") String name){
        return Response.ok().entity(name).build();
    }

    @POST
    @Path("/{name}/2")
    public Response test2(@PathParam("name") String name){
        return Response.status(Response.Status.FORBIDDEN).build();
    }

    @GET
    @Path("/person")
    @Produces("application/json")
    public Response getPerson(){
        Gson gson = new Gson();
        String json = gson.toJson(PersonFactory.createPerson());
        return Response.ok().entity(json).build();
    }
}
