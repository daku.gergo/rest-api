package hu.evo.training.dakug;

public class PersonFactory {

    public static Person createPerson(){
        Person child = new Person("Nevtelen Lajos", 25);
        Person adult = new Person("Nevtelen Marika", 57);
        Person child2 = new Person("Nevtelen János", 40);
        adult.addChild(child);
        adult.addChild(child2);
        return adult;
    }
}
